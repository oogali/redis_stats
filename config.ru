$: << File.dirname(__FILE__) + '/lib' unless $:.include? File.dirname(__FILE__) + '/lib'

require 'rubygems'
require 'redis'
require 'sinatra'
require 'haml'
require 'redisstats/redisstats'

# set working directory
working = File.expand_path File.dirname(__FILE__)
set :root, working

# set haml to html5 mode, disable sinatra from autostart
set :haml, :format => :html5
disable :run, :reload

# redirect logs
log = File.new(File.join(working, 'run', 'sinatra.log'), 'a')
$stdout.reopen(log)
$stderr.reopen(log)

# load core class and run
run RedisStats::Application
