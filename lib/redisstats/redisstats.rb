require 'sinatra'
require 'sinatra/async'
require 'sinatra/settings'
require 'haml'
require 'json'

module RedisStats
  class Application < Sinatra::Application
    register Sinatra::Async
    configure :development do
      register Sinatra::Settings
      enable :show_settings
    end

    helpers do
      def interval(secs, negative = false)
        days = secs / 86400
        hours = secs % 86400 / 3600
        minutes = secs % 3600 / 60
        seconds = secs % 60

        if negative
          interval_str = secs == -1 ? 'Not Set' : -1
        else
          if days > 0
            interval_str = "#{days} day#{days == 1 ? '' : 's' if days > 0}"
            interval_str += ", #{hours} hour#{hours == 1 ? '' : 's' if hours > 0}" if hours > 0
          elsif hours > 0
            interval_str = "#{hours} hour#{hours == 1 ? '' : 's' if hours > 0}"
            interval_str += ", #{minutes} minute#{minutes == 1 ? '' : 's' if minutes > 0}" if minutes > 0
          elsif minutes > 0
            interval_str = "#{minutes} minute#{minutes == 1 ? '' : 's' if minutes > 0}"
            interval_str += ", #{seconds} second#{seconds == 1 ? '' : 's' if seconds > 0}" if seconds > 0
          else
            interval_str = "#{seconds} second#{seconds == 1 ? '' : 's' if seconds > 0}"
          end
        end

        interval_str
      end

      def size_unit(bytes)
        return "#{'%g' % ('%.2f' % (bytes / (1024*1048576).to_f))}GB" if bytes >= (1024*1048576) / 2
        return "#{'%g' % ('%.1f' % (bytes / 1048576.to_f))}MB" if bytes >= 1048576 / 2
        return "#{'%g' % ('%.1f' % (bytes / 1024.to_f))}KB" if bytes >= 1024 / 2
        return "#{bytes} bytes"
      end

      def comma(number, delimiter = ',')
        number.to_s.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1#{delimiter}").reverse
      end

      def pie_values(mem)
        "#{mem[:fragment]},#{mem[:used]},#{mem[:free]}"
      end

      def get_server_info(server, port = 6379, password = nil)
        stats = {}
        puts "requested server #{server}, port #{port}, password #{password}"

        begin
          r = Redis.new(:host => server, :port => port || 6379, :password => password)
        rescue => err
          puts "Could not connect to #{server}:#{port}: #{err.to_s}"
          return nil
        end

        max_memory = r.config('get', 'maxmemory').last.to_i rescue nil
        server_info = r.info

        stats = {
          :hostname => server,
          :uptime => interval(server_info['uptime_in_seconds'].to_i),
          :memory => {
            :summary => "#{size_unit(server_info['used_memory'].to_i)} #{max_memory > 0 ? ('of ' + size_unit(max_memory)) : '(unlimited)' }",
            :used => server_info['used_memory'].to_i,
            :free => max_memory > 0 ? (max_memory - server_info['used_memory'].to_i) : 0,
            :fragment => server_info['mem_fragmentation_ratio'].to_f >= 1 ? (server_info['used_memory_rss'].to_i - server_info['used_memory'].to_i) : 0
          },
          :commands => comma(server_info['total_commands_processed'].to_i),
          :fragment => server_info['mem_fragmentation_ratio'].to_f,
          :keys => comma(server_info.keys.reject { |x| x if not x.match /db\d+/ }.reduce(0) { |s,v| s + server_info[v].match(/keys=(\d+)/)[1].to_i })
        }

        stats.keys.length > 0 ? stats : nil
      end

      def hostport(target)
        host, port = target.split(/:/, 2)
        port = 6379 if !port or port.to_i <= 0
        [ host, port ]
      end

      def poll_server(target = 'localhost')
        host, port = hostport target
        get_server_info host, port
      end

      def get_keys(target = 'localhost', limit = -1, start = 0)
        server, port = hostport target
        password = nil

        begin
          r = Redis.new(:host => server, :port => port || 6379, :password => password)
          r.keys[start..limit]
        rescue => err
          puts "Could not connect to #{server}:#{port}: #{err.to_s}"
          return nil
        end
      end

      def get_key_info(key, target = 'localhost')
        server, port = hostport target
        password = nil

        begin
          r = Redis.new(:host => server, :port => port || 6379, :password => password)
          info = {
            :type => r.type(key).capitalize,
            :ttl => interval(r.ttl(key), true)
          }

          case info[:type]
            when 'String'
              info[:size] = size_unit r.strlen(key)
              info[:data] = r.get(key)

            when 'Hash'
              info[:size] = "%d keys" % r.hlen(key)
              info[:data] = r.hkeys(key).sort.join(', ')

            when 'List'
              info[:size] = "%d items" % r.llen(key)
              info[:data] = r.lrange(key, 0, info[:size] - 1).sort.join(', ')

            when 'Set'
              info[:data] = r.smembers(key)
              info[:size] = "%d members" % info[:data]
              info[:data] = info[:data].sort.join(', ')
          end

          info
        rescue => err
          puts "Could not connect to #{server}:#{port}: #{err.to_s}"
          return nil
        end
      end
    end

    get '/' do
      begin
        @server = poll_server
        body { haml :index }
      rescue => err
        @hostname = 'localhost'
        @error = err.to_s
        body { haml :error }
      end
    end

    get '/s/:server' do
      begin
        @server = poll_server params[:server]
        body { haml :index }
      rescue => err
        @hostname = params[:server]
        @error = err.to_s
        body { haml :error }
      end
    end

    get '/k/:server' do
      @hostname = params[:server]

      begin
        @keys = get_keys(@hostname)
        @total = comma @keys.length
        @keys = @keys[0..9]
        body { haml :keys }
      rescue => err
        @error = err.to_s
        body { haml :error }
      end
    end

    get '/k.json/:server' do
      @hostname = params[:server]

      begin
        content_type 'application/javascript', :charset => 'utf-8'
        @keys = get_keys @hostname
        body { JSON.pretty_generate(@keys) + "\n" }
      rescue => err
        body { JSON.pretty_generate({ :error => err.to_s }) + "\n" }
      end
    end

    get '/k.info/:server' do
      hostname = params[:server]
      key = params[:key]

      begin
        content_type 'application/javascript', :charset => 'utf-8'
        body { JSON.pretty_generate(get_key_info key, hostname) + "\n" }
      rescue => err
        body { JSON.pretty_generate({ :error => err.to_s }) + "\n" }
      end
    end

    get '/k.srch/:server' do
      @hostname = params[:server]
      @query = params[:search]
      limit = (params[:limit] || 10).to_i
      start = (params[:start] || 0).to_i

      begin
        content_type 'application/javascript', :charset => 'utf-8'
        @keys = get_keys(@hostname)
        @keys.reject! { |x| x if not x.match @query }
        total = comma @keys.length

        body {
          JSON.pretty_generate({
            :start => start,
            :limit => limit < @keys.length ? limit : total,
            :total => total,
            :keys => @keys.sort[start..(start + limit - 1)]
          }) + "\n"
        }
      rescue => err
        body { JSON.pretty_generate({ :error => err.to_s }) + "\n" }
      end
    end

    post '/k.srch/:server' do
      @hostname = params[:server]
      @query = params[:search]

      begin
        @keys = get_keys(@hostname).reject { |x| x if not x.match @query }
        body { haml :keys }
      rescue => err
        @error = err.to_s
        body { haml :error }
      end
    end

    get '/s.json/:server' do
      begin
        @server = poll_server params[:server]
        content_type 'application/javascript', :charset => 'utf-8'
        body { JSON.pretty_generate(@server) + "\n" }
      rescue => err
        body { JSON.pretty_generate({ :error => err.to_s }) + "\n" }
      end
    end

    aget '/css/default.css' do
      content_type 'text/css', :charset => 'utf-8'
      body { sass :default }
    end

    aget '/css/reset.css' do
      content_type 'text/css', :charset => 'utf-8'
      body { sass :reset }
    end

    aget '/css/scrollbar.css' do
      content_type 'text/css', :charset => 'utf-8'
      body { sass :scrollbar }
    end
  end
end
